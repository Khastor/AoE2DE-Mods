# Age of Empires II Definitive Edition Mods

Mods for Age of Empires II Definitive Edition.

## Age of Smurfs - Huntable Smurfs

![Age of Smurfs - Huntable Smurfs thumbnail](mods/age_of_smurfs_huntable_smurfs/thumbnail/thumbnail.png "Thumbnail")

Age of Empires II © Microsoft Corporation. Age of Smurfs - Huntable Smurfs was created under Microsoft's "[Game Content Usage Rules](https://www.xbox.com/en-us/developers/rules)" using assets from Age of Empires II, and it is not endorsed by or affiliated with Microsoft.

### Description

Say goodbye to boring deer and hello to a colorful parade of huntable Smurfs frolicking across the map in their signature blue hue!

Find it on the [mod center](https://www.ageofempires.com/mods/details/212087/).

### Features

- Graphics for deer, gazelles, ibex, ostriches and zebras are replaced with Smurfs graphics.
- Compatible with enhanced graphics (UHD).
