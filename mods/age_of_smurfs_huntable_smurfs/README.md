## Age of Smurfs - Huntable Smurfs

![Age of Smurfs - Huntable Smurfs thumbnail](thumbnail/thumbnail.png "Thumbnail")

Age of Empires II © Microsoft Corporation. Age of Smurfs - Huntable Smurfs was created under Microsoft's "[Game Content Usage Rules](https://www.xbox.com/en-us/developers/rules)" using assets from Age of Empires II, and it is not endorsed by or affiliated with Microsoft.

### Description

Say goodbye to boring deer and hello to a colorful parade of huntable Smurfs frolicking across the map in their signature blue hue!

Find it on the [mod center](https://www.ageofempires.com/mods/details/212087/).

### Features

- Graphics for deer, gazelles, ibex, ostriches and zebras are replaced with Smurfs graphics.
- Compatible with enhanced graphics (UHD).

### How to build

#### Dependencies

- [Blender](https://www.blender.org/) The source file was created using Blender 4.0
- [SLX Studio](https://ageofempires.fandom.com/wiki/SLX_Studio) To compress the rendered animation frames into SMX graphics files
- [Python 3](https://www.python.org/) To run the configuration and installation scripts

#### Step 1 - Render the animations

Open age_of_smurfs_huntable_smurfs.blend in Blender and render each scene as an animation.
This will create a directory for each animation, containing all rendered frame colors and masks.

#### Step 2 - Generate SMX files

Run configure.py to generate SLX files for all the animations rendered in the previous step.
Open SLX Studio and batch convert SLX files to SMX files in the current directory with the recursive option enabled.

#### Step 3 - Package the mod

Run install.py to package all SMX files into the final directory structure of the mod.
This creates the final directory structure of the mod in the current directory.

### Technical

The number of frames and duration of an animation is not the same for all passive huntable graphics.
If an animation has the same number of frames and a similar duration for multiple units, this animation will be rendered only once, then copied to all similar graphics targets by the install.py script.

| unit                  | death animation (frames/duration) | decay animation (frames/duration) | idleA animation (frames/duration) | idleB animation (frames/duration) | run animation (frames/duration) | walk animation (frames/duration) |
| :---                  | :---:                             | :---:                             | :---:                             | :---:                             | :---:                           | :---:                            |
| a_hunt_deer           | 45/1.4s                           | 30/30s                            | 60/3s                             | 60/3s                             | 30/0.5s                         | 30/0.75s                         |
| a_hunt_gazelle        | 90/1.4s                           | 30/30s                            | 60/3s                             | -                                 | 30/0.5s                         | 90/0.75s                         |
| a_hunt_ibex           | 30/1.4s                           | 30/30s                            | 60/3s                             | -                                 | 30/0.54s                        | 30/0.56s                         |
| a_hunt_ostrich        | 30/1.4s                           | 30/30s                            | 60/4.125s                         | 60/4.125s                         | 30/0.5s                         | 45/0.75s                         |
| a_hunt_zebra          | 30/1.25s                          | 60/30s                            | 60/2.475s                         | -                                 | 30/0.54s                        | 30/0.54s                         |

### Gallery

![Age of Smurfs - Huntable Smurfs screenshot 1](gallery/screenshot_01.jpg "Screenshot 1")

![Age of Smurfs - Huntable Smurfs screenshot 2](gallery/screenshot_02.jpg "Screenshot 2")

![Age of Smurfs - Huntable Smurfs screenshot 3](gallery/screenshot_03.jpg "Screenshot 3")

![Age of Smurfs - Huntable Smurfs screenshot 4](gallery/screenshot_04.jpg "Screenshot 4")

![Age of Smurfs - Huntable Smurfs screenshot uhd 1](gallery/screenshot_uhd_01.jpg "Screenshot UHD 1")

![Age of Smurfs - Huntable Smurfs screenshot uhd 2](gallery/screenshot_uhd_02.jpg "Screenshot UHD 2")

![Age of Smurfs - Huntable Smurfs screenshot uhd 3](gallery/screenshot_uhd_03.jpg "Screenshot UHD 3")

![Age of Smurfs - Huntable Smurfs screenshot uhd 4](gallery/screenshot_uhd_04.jpg "Screenshot UHD 4")
