import os
import shutil

animations_x1 = [
    # a_hunt_deer
    ("a_hunt_deer_deathA_x1", []),
    ("a_hunt_deer_decayA_x1", []),
    ("a_hunt_deer_idleA_x1", []),
    ("a_hunt_deer_idleB_x1", ["a_hunt_deer_idleA_x1"]),
    ("a_hunt_deer_runA_x1", []),
    ("a_hunt_deer_walkA_x1", []),
    # a_hunt_gazelle
    ("a_hunt_gazelle_deathA_x1", []),
    ("a_hunt_gazelle_decayA_x1", ["a_hunt_deer_decayA_x1"]),
    ("a_hunt_gazelle_idleA_x1", ["a_hunt_deer_idleA_x1"]),
    ("a_hunt_gazelle_runA_x1", ["a_hunt_deer_runA_x1"]),
    ("a_hunt_gazelle_walkA_x1", []),
    # a_hunt_ibex
    ("a_hunt_ibex_deathA_x1", []),
    ("a_hunt_ibex_decayA_x1", ["a_hunt_deer_decayA_x1"]),
    ("a_hunt_ibex_idleA_x1", ["a_hunt_deer_idleA_x1"]),
    ("a_hunt_ibex_runA_x1", ["a_hunt_deer_runA_x1"]),
    ("a_hunt_ibex_walkA_x1", ["a_hunt_deer_walkA_x1"]),
    # a_hunt_ostrich
    ("a_hunt_ostrich_deathA_x1", ["a_hunt_ibex_deathA_x1"]),
    ("a_hunt_ostrich_decayA_x1", ["a_hunt_deer_decayA_x1"]),
    ("a_hunt_ostrich_idleA_x1", ["a_hunt_deer_idleA_x1"]),
    ("a_hunt_ostrich_idleB_x1", ["a_hunt_deer_idleA_x1"]),
    ("a_hunt_ostrich_runA_x1", ["a_hunt_deer_runA_x1"]),
    ("a_hunt_ostrich_walkA_x1", []),
    # a_hunt_zebra
    ("a_hunt_zebra_deathA_x1", ["a_hunt_ibex_deathA_x1"]),
    ("a_hunt_zebra_decayA_x1", []),
    ("a_hunt_zebra_idleA_x1", ["a_hunt_deer_idleA_x1"]),
    ("a_hunt_zebra_runA_x1", ["a_hunt_deer_runA_x1"]),
    ("a_hunt_zebra_walkA_x1", ["a_hunt_deer_walkA_x1"])
]

animations_x2 = [
    # a_hunt_deer
    ("a_hunt_deer_deathA_x2", []),
    ("a_hunt_deer_decayA_x2", []),
    ("a_hunt_deer_idleA_x2", []),
    ("a_hunt_deer_idleB_x2", ["a_hunt_deer_idleA_x2"]),
    ("a_hunt_deer_runA_x2", []),
    ("a_hunt_deer_walkA_x2", []),
    # a_hunt_gazelle
    ("a_hunt_gazelle_deathA_x2", []),
    ("a_hunt_gazelle_decayA_x2", ["a_hunt_deer_decayA_x2"]),
    ("a_hunt_gazelle_idleA_x2", ["a_hunt_deer_idleA_x2"]),
    ("a_hunt_gazelle_runA_x2", ["a_hunt_deer_runA_x2"]),
    ("a_hunt_gazelle_walkA_x2", []),
    # a_hunt_ibex
    ("a_hunt_ibex_deathA_x2", []),
    ("a_hunt_ibex_decayA_x2", ["a_hunt_deer_decayA_x2"]),
    ("a_hunt_ibex_idleA_x2", ["a_hunt_deer_idleA_x2"]),
    ("a_hunt_ibex_runA_x2", ["a_hunt_deer_runA_x2"]),
    ("a_hunt_ibex_walkA_x2", ["a_hunt_deer_walkA_x2"]),
    # a_hunt_ostrich
    ("a_hunt_ostrich_deathA_x2", ["a_hunt_ibex_deathA_x2"]),
    ("a_hunt_ostrich_decayA_x2", ["a_hunt_deer_decayA_x2"]),
    ("a_hunt_ostrich_idleA_x2", ["a_hunt_deer_idleA_x2"]),
    ("a_hunt_ostrich_idleB_x2", ["a_hunt_deer_idleA_x2"]),
    ("a_hunt_ostrich_runA_x2", ["a_hunt_deer_runA_x2"]),
    ("a_hunt_ostrich_walkA_x2", []),
    # a_hunt_zebra
    ("a_hunt_zebra_deathA_x2", ["a_hunt_ibex_deathA_x2"]),
    ("a_hunt_zebra_decayA_x2", []),
    ("a_hunt_zebra_idleA_x2", ["a_hunt_deer_idleA_x2"]),
    ("a_hunt_zebra_runA_x2", ["a_hunt_deer_runA_x2"]),
    ("a_hunt_zebra_walkA_x2", ["a_hunt_deer_walkA_x2"])
]

def install_smx_asset(install_filename, title, fallbacks):
    print("[Info] Installing {} -> {}".format(title, install_filename))
    source_filename = "{}.smx".format(title)
    if os.path.isfile(source_filename):
        print("[Info] Copying {} -> {}".format(source_filename, install_filename))
        shutil.copyfile(source_filename, install_filename)
        return True
    else:
        print("[Info] File not found: {}".format(source_filename))
        for fallback_title in fallbacks:
            if install_smx_asset(install_filename, fallback_title, []):
                return True
        return False

def install_smx_assets(install_directory, animations):
    for title, fallbacks in animations:
        if not install_smx_asset("{}/{}.smx".format(install_directory, title), title, fallbacks):
            print("[Error] Failed to install {}".format(title))

if __name__ == "__main__":
    install_directory = "resources/_common/drs/graphics"
    if not os.path.isdir(install_directory):
        print("[Info] Create directory: {}".format(install_directory))
        os.makedirs(install_directory)
    print("[Info] Installing assets in: {}".format(install_directory))
    install_smx_assets(install_directory, animations_x1)
    install_smx_assets(install_directory, animations_x2)
