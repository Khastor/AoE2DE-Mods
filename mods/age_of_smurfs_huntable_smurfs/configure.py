import os

animations_x1 = [
    # a_hunt_deer
    ("a_hunt_deer_deathA_x1", 45, 16, 66, 66),
    ("a_hunt_deer_decayA_x1", 30, 16, 66, 66),
    ("a_hunt_deer_idleA_x1", 60, 16, 66, 66),
    ("a_hunt_deer_idleB_x1", 60, 16, 66, 66),
    ("a_hunt_deer_runA_x1", 30, 16, 66, 66),
    ("a_hunt_deer_walkA_x1", 30, 16, 66, 66),
    # a_hunt_gazelle
    ("a_hunt_gazelle_deathA_x1", 90, 16, 66, 66),
    ("a_hunt_gazelle_decayA_x1", 30, 16, 66, 66),
    ("a_hunt_gazelle_idleA_x1", 60, 16, 66, 66),
    ("a_hunt_gazelle_runA_x1", 30, 16, 66, 66),
    ("a_hunt_gazelle_walkA_x1", 90, 16, 66, 66),
    # a_hunt_ibex
    ("a_hunt_ibex_deathA_x1", 30, 16, 66, 66),
    ("a_hunt_ibex_decayA_x1", 30, 16, 66, 66),
    ("a_hunt_ibex_idleA_x1", 60, 16, 66, 66),
    ("a_hunt_ibex_runA_x1", 30, 16, 66, 66),
    ("a_hunt_ibex_walkA_x1", 30, 16, 66, 66),
    # a_hunt_ostrich
    ("a_hunt_ostrich_deathA_x1", 30, 16, 66, 66),
    ("a_hunt_ostrich_decayA_x1", 30, 16, 66, 66),
    ("a_hunt_ostrich_idleA_x1", 60, 16, 66, 66),
    ("a_hunt_ostrich_idleB_x1", 60, 16, 66, 66),
    ("a_hunt_ostrich_runA_x1", 30, 16, 66, 66),
    ("a_hunt_ostrich_walkA_x1", 45, 16, 66, 66),
    # a_hunt_zebra
    ("a_hunt_zebra_deathA_x1", 30, 16, 66, 66),
    ("a_hunt_zebra_decayA_x1", 60, 16, 66, 66),
    ("a_hunt_zebra_idleA_x1", 60, 16, 66, 66),
    ("a_hunt_zebra_runA_x1", 30, 16, 66, 66),
    ("a_hunt_zebra_walkA_x1", 30, 16, 66, 66)
]

animations_x2 = [
    # a_hunt_deer
    ("a_hunt_deer_deathA_x2", 45, 16, 132, 132),
    ("a_hunt_deer_decayA_x2", 30, 16, 132, 132),
    ("a_hunt_deer_idleA_x2", 60, 16, 132, 132),
    ("a_hunt_deer_idleB_x2", 60, 16, 132, 132),
    ("a_hunt_deer_runA_x2", 30, 16, 132, 132),
    ("a_hunt_deer_walkA_x2", 30, 16, 132, 132),
    # a_hunt_gazelle
    ("a_hunt_gazelle_deathA_x2", 90, 16, 132, 132),
    ("a_hunt_gazelle_decayA_x2", 30, 16, 132, 132),
    ("a_hunt_gazelle_idleA_x2", 60, 16, 132, 132),
    ("a_hunt_gazelle_runA_x2", 30, 16, 132, 132),
    ("a_hunt_gazelle_walkA_x2", 90, 16, 132, 132),
    # a_hunt_ibex
    ("a_hunt_ibex_deathA_x2", 30, 16, 132, 132),
    ("a_hunt_ibex_decayA_x2", 30, 16, 132, 132),
    ("a_hunt_ibex_idleA_x2", 60, 16, 132, 132),
    ("a_hunt_ibex_runA_x2", 30, 16, 132, 132),
    ("a_hunt_ibex_walkA_x2", 30, 16, 132, 132),
    # a_hunt_ostrich
    ("a_hunt_ostrich_deathA_x2", 30, 16, 132, 132),
    ("a_hunt_ostrich_decayA_x2", 30, 16, 132, 132),
    ("a_hunt_ostrich_idleA_x2", 60, 16, 132, 132),
    ("a_hunt_ostrich_idleB_x2", 60, 16, 132, 132),
    ("a_hunt_ostrich_runA_x2", 30, 16, 132, 132),
    ("a_hunt_ostrich_walkA_x2", 45, 16, 132, 132),
    # a_hunt_zebra
    ("a_hunt_zebra_deathA_x2", 30, 16, 132, 132),
    ("a_hunt_zebra_decayA_x2", 60, 16, 132, 132),
    ("a_hunt_zebra_idleA_x2", 60, 16, 132, 132),
    ("a_hunt_zebra_runA_x2", 30, 16, 132, 132),
    ("a_hunt_zebra_walkA_x2", 30, 16, 132, 132)
]

def create_slx_assets(animations):
    for title, frames, orientations, hotspot_x, hotspot_y in animations:
        if os.path.isdir(title):
            with open("{0}/{0}.slx".format(title), "w") as slx_file:
                slx_frames = frames * orientations + 1
                slx_file.write("SLX 0.5\n{}\n".format(slx_frames))
                for frame_index in range(slx_frames):
                    slx_file.write("{},{}\n".format(hotspot_x, hotspot_y))
                    slx_file.write("frame_mask{:04d}.bmp\n".format(frame_index + 1))
                    slx_file.write("frame_color{:04d}.bmp\n".format(frame_index + 1))

if __name__ == "__main__":
    create_slx_assets(animations_x1)
    create_slx_assets(animations_x2)
